## Ejercicio 3. Análisis general
* ¿Cuántos paquetes componen la captura?: 1268
* ¿Cuánto tiempo dura la captura?: 12.81s
* Aproximadamente, ¿cuántos paquetes se envían por segundo?: 0.01 ## Time % Nº paquetes
* ¿Qué protocolos de nivel de red aparecen en la captura?: IP
* ¿Qué protocolos de nivel de transporte aparecen en la captura?: UDP
* ¿Qué protocolos de nivel de aplicación aparecen en la captura?: RTP
## Ejercicio 4. Análisis de un paquete
* Número aleatorio elegido (N): 13
* Dirección IP de la máquina B: 192.168.0.10
* Puerto UDP desde el que se envía el paquete: 54550
* Puerto UDP hacia el que se envía el paquete: 49154
* SSRC del paquete: 834543118
* Tipo de datos en el paquete RTP (según indica el campo correspondiente):ITU-T G.711 PCMU (0)
## Ejercicio 5. Análisis de un flujo (stream)
* ¿Cuál es el primer número de tu DNI / NIE?: 5
* ¿Cuántos paquetes hay en el flujo F?: 642
* ¿El origen del flujo F es la máquina A o B?: A
* ¿Cuál es el puerto UDP de destino del flujo F?: 49154
* ¿Cuántos segundos dura el flujo F?: 0.056
## Ejercicio 6. Métricas del flujo
* ¿Cuál es la diferencia máxima en el flujo?: 31.65 ##Max delta
* ¿Cuál es el jitter medio del flujo?: 12.23 ##Mean Jitter
* ¿Cuántos paquetes del flujo se han perdido?: 0%
* ¿Cuánto tiempo (aproximadamente) está el jitter por encima de 0.4 ms?:
## Ejercicio 7. Métricas de un paquete RTP
* ¿Cuál es su número de secuencia dentro del flujo RTP?: 26535
* ¿Ha llegado pronto o tarde, con respecto a cuando habría debido llegar?: Pronto
* ¿Qué jitter se ha calculado para ese paquete?: 4.37ms
* ¿Qué timestamp tiene ese paquete?: 29.897 ##Delta
* ¿Por qué número hexadecimal empieza sus datos de audio?: 0.000000 s @ 1 ##Start at
## Ejercicio 8. Captura de una traza RTP
* Salida del comando `date`: lun 23 oct 2023 17:43:45 CEST
* Número total de paquetes en la captura: 3564
* Duración total de la captura (en segundos): 16.67s
## Ejercicio 9. Análisis de la captura realizada
* ¿Cuántos paquetes RTP hay en el flujo?: 169
* ¿Cuál es el SSRC del flujo?: 0x93949596
* ¿En qué momento (en ms) de la traza comienza el flujo?: 0.63 ms
* ¿Qué número de secuencia tiene el primer paquete del flujo?: 34974
* ¿Cuál es el jitter medio del flujo?: 0.0
* ¿Cuántos paquetes del flujo se han perdido?: 0
## Ejercicio 10 (segundo periodo). Analiza la captura de un compañero
* ¿De quién es la captura que estás usando?: Hao
* ¿Cuántos paquetes de más o de menos tiene que la tuya? (toda la captura): 5498 - 3564 = 1934 de mas
* ¿Cuántos paquetes de más o de menos tiene el flujo RTP que el tuyo?: 219 -169 = 50 de mas
* ¿Qué diferencia hay entre el número de secuencia del primer paquete de su flujo RTP con respecto al tuyo?: suyo=15301, mio=56407
* En general, ¿que diferencias encuentras entre su flujo RTP y el tuyo?: Las principales diferencias que podemos encontrar estan en los campos= Contenido de los Datos, número de Paquetes, número de Secuencia , timestamps... entre otros
